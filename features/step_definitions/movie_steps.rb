# Add a declarative step here for populating the DB with movies.

Given /the following movies exist/ do |movies_table|
  movies_table.hashes.each do |movie|
  #film = movie.inject({}){|memo, (k, v)| memo[k.to_sym] = v; memo}
#       each returned element will be a hash whose key is the table header.
    Movie.create!(movie)
#     you should arrange to add that movie to the database here.
  end
end

# Make sure that one string (regexp) occurs before or after another one
#   on the same page

Then /I should see "(.*)" before "(.*)"/ do |e1, e2|
  if page.body == /.*"#{e1}".*"#{e2}".*/
  end
  #  ensure that that e1 occurs before e2.
  #  page.body is the entire content of the page as a string.
  flunk "Unimplemented"
end

When /I (un)?check the following ratings '(.*)'/ do |uncheck, rating_list|
  allratings = ["PG", "PG-13", "G", "R"]
    allratings.each do |rat|
      steps %Q{
	      When I uncheck "#{rat}"
      }
    end
    
  split_ratings = rating_list.split(%r{,\s*})
  split_ratings.each do |rating|
    steps %Q{
      When I #{uncheck}check "#{rating}"
    }
  end
end


Then /^I should only see movies with ratings '(.*)'/ do |rating_list|
    split_ratings = rating_list.split(%r{,\s*})
    split_ratings.each do |rating|
      assert page.body.include?(">#{rating}<")
    end
end

Then /^I should not see movies with ratings other than '(.*)'$/ do |rating_list|
  split_ratings = rating_list.split(%r{,\s*})
  notratings = ["PG", "PG-13", "G", "R"]
    split_ratings.each do |del|
      notratings.delete_at(notratings.index(del))
    end
  notratings.each do |rating|
    assert page.body.include?(">#{rating}<") != true
  end
end
